<?php
function select_usuarios_conectados($hoy){

	$link =  mysqli_connect('192.168.5.27', 'root', 'tec.wor_08');
	mysqli_select_db($link,'Sineduc_usuarios_conectados');

	$query ="(
	Select
	date_format(from_unixtime(uc.uco_time), '%H:%i') as HORA,
	uc.uco_cantidad_usuario CANT,
	1 as SYSTEM
	FROM
	usuarios_conectados uc
	WHERE
	date_format(from_unixtime(uc.uco_time),'%Y-%m-%d') = '$hoy'
	) UNION (
	Select
	date_format(from_unixtime(mc.uco_time), '%H:%i') as HORA,
	mc.uco_cantidad_mateo CANT,
	2 as SYSTEM
	FROM
	mateo_conectados mc
	WHERE
	date_format(from_unixtime(mc.uco_time),'%Y-%m-%d') = '$hoy'
	)
	UNION (
	Select
	date_format(from_unixtime(ac.uco_time), '%H:%i') as HORA,
	ac.uco_cantidad_apoderado CANT,
	9 as SYSTEM
	FROM
	apoderados_conectados ac
	WHERE
	date_format(from_unixtime(ac.uco_time),'%Y-%m-%d') = '$hoy'
	)
	UNION (
        Select
        date_format(from_unixtime(ac.uco_time), '%H:%i') as HORA,
        ac.uco_cantidad_pedagogica CANT,
        6 as SYSTEM
        FROM
        pedagogica_conectados ac
        WHERE
        date_format(from_unixtime(ac.uco_time),'%Y-%m-%d') = '$hoy'
        )
	;

	";

	$arrUser = array();
	$res = mysqli_query($link, $query);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$arrUser[$row["HORA"]][$row["SYSTEM"]] = $row["CANT"];
	}
	mysqli_free_result($res);
	mysqli_close($link);

	$arrSystem[1] = "SND";
	$arrSystem[2] = "MATEONET";
	$arrSystem[9] = "APODERADO SND";
	$arrSystem[6] = "PEDAGOGICA";

	$snd=0;
	$mateo=0;
	$aposnd =0;
	foreach ($arrUser as $hora => $sysdata){

		$snd = ($sysdata[1])?$sysdata[1]:0;
		$mateo = ($sysdata[2])?$sysdata[2]:0;
		$aposnd = ($sysdata[9])?$sysdata[9]:0;
		$peda = ($sysdata[6])?$sysdata[6]:0;

		$arrRes[] = "['". $hora ."', ". $snd .", ". $mateo .", ".$aposnd.", ".$peda."]";
		$arrRes2[] = "{ hora: '". $hoy." ".$hora ."', napsis: ". $snd .", mateonet: ". $mateo .", apoderados:  ".$aposnd.", pedagogica: ".$peda."}";
	}

	$rs["RES1"] = $arrRes;
	$rs["RES2"] = $arrRes2;
// 			echo "<pre>";
// 			print_r($rs);
// 			echo "</pre>";
	return $rs;
}

function asterisk($hoy,$seg = 30){

	$link =  mysqli_connect('192.168.7.31', 'inexoos', 'inx@2016');
	mysqli_select_db($link,'asteriskcdrdb');

	$data = array();

	$sql1 = 'SELECT
				HOUR(c.calldate) as "HORA",
				c.disposition as "DISP",
				SUM(c.duration) AS SUMA,
				COUNT(*) as CANT
			FROM asteriskcdrdb.cdr as c
			/*LEFT JOIN asterisk.users as u ON u.extension = c.dst*/
			WHERE DATE(c.calldate) = curdate()
			AND c.dst in ( 413,414,416,425,427,458,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029)
			AND c.duration != 0
			GROUP BY HOUR(c.calldate),c.disposition
			ORDER BY c.calldate ASC;';
	$res = mysqli_query($link, $sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$suma[$row["HORA"]]["ALL"] = $suma[$row["HORA"]]["ALL"] + $row["CANT"];
		if($row["DISP"] == "ANSWERED"){
			$suma[$row["HORA"]]["ANSWERED"] = $row["CANT"];
		}else{
			$suma[$row["HORA"]]["NOANSWERED"] = $suma[$row["HORA"]]["NOANSWERED"] + $row["CANT"];
		}

	}

	foreach ($suma as $hora => $dd){

		if(!$dd["ANSWERED"]){
			$ANSWERED = 0;
		}else{
			$ANSWERED = $dd["ANSWERED"];
		}

		if(!$dd["NOANSWERED"]){
			$NOANSWERED = 0;
		}else{
			$NOANSWERED = $dd["NOANSWERED"];
		}


		$arrData[] = "{ y: '".$hora."', a: ".$dd["ALL"].", b:".$NOANSWERED.", c: ".$ANSWERED." }";

	}


	$data["ALL_CALL"] = implode(",", $arrData) ;



	$sql2 = 'SELECT
					count(*) as llamadas, c.disposition
				FROM asteriskcdrdb.cdr as c
				/*INNER JOIN asterisk.users as u ON u.extension = c.dst*/
				WHERE DATE(c.calldate) = curdate()
				AND c.dst in (413,414,416,425,427,458,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029)
				AND c.duration != 0
					group by c.disposition
				 order by c.calldate desc;';
	$res = mysqli_query($link, $sql2);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data["COUNT_LLAMADAS"][$row["disposition"]] = $row["llamadas"];
	}

#	$sql3 = "SELECT
#				c.calldate as 'Fecha',
#				c.src as 'Desde',
#				c.dst as 'Hacia',
#				SUM(c.duration)/60 as 'Minutos',
#				count(*) as 'Total',
#				ROUND(avg(c.duration)/60,1) as 'Promedio',
#				if(perdidas.cantLlamadasPerdidas is null,0,perdidas.cantLlamadasPerdidas) as cantLlamadasPerdidas,
#				c.disposition as 'Disponibilidad',
#				u.name as 'Nombres'
#				FROM asteriskcdrdb.cdr as c
#				/*LEFT JOIN asterisk.users as u ON u.extension = c.dst*/
#			    LEFT JOIN (
#               SELECT	u.extension,count(u.extension) as 'cantLlamadasPerdidas'
#			FROM asteriskcdrdb.cdr as c
#            /*INNER JOIN asterisk.users as u ON u.extension = c.dst*/
#             WHERE DATE(c.calldate) = curdate()
#            AND c.dst in (413,414,416,425,427,458,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029)
#            AND LENGTH(c.src) > 3
#            AND c.duration != 0
#			AND c.disposition  LIKE 'NO ANSWER'
#			group by u.extension
#                ) as perdidas on perdidas.extension = u.extension
#				WHERE DATE(c.calldate) = curdate()
#				AND c.dst in (413,414,416,425,427,458,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029)
#				AND LENGTH(c.src) > 3
#			AND c.billsec >= '".$seg."' AND c.disposition = 'ANSWERED'
#				GROUP BY c.dst ORDER BY AVG(c.billsec) ASC, SUM(c.billsec) ASC, count(*) ASC  LIMIT 0,10";

$sql3 = "SELECT
    c.calldate as 'Fecha',
    c.src as 'Desde',
    c.dst as 'Hacia',
    SUM(c.duration) / 60 as 'Minutos',
    count(*) as 'Total',
    ROUND(avg(c.duration) / 60, 1) as 'Promedio',
    if(perdidas.cantLlamadasPerdidas is null,
        0,
        perdidas.cantLlamadasPerdidas) as cantLlamadasPerdidas,
    c.disposition as 'Disponibilidad',
    /*c.dst as 'Nombres'*/
    IF(c.dst = 1020,'Felipe Gonzalez',IF(c.dst = 1021,'Edison Fernandez',IF(c.dst = 1022,'Luis Morales',IF(c.dst = 1023,'Samantha Melillán',IF(c.dst = 1024,'Tamara León del Prado',IF(c.dst = 1025,'Carlos Yañez',c.dst)))))) as 'Nombres'

FROM
    asteriskcdrdb.cdr as c
        LEFT JOIN
    (SELECT
        c.dst, count(c.dst) as 'cantLlamadasPerdidas'
    FROM
        asteriskcdrdb.cdr as c
    WHERE
        DATE(c.calldate) = curdate()
            AND c.dst in (413 , 414, 416, 425, 427, 458, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029)
            AND LENGTH(c.src) > 3
            AND c.duration != 0
            AND c.disposition LIKE 'NO ANSWER'
    group by c.dst) as perdidas ON c.dst
WHERE
    DATE(c.calldate) = curdate()
        AND c.dst in (413 , 414,
        416,
        425,
        427,
        458,
        1020,
        1021,
        1022,
        1023,
        1024,
        1025,
        1026,
        1027,
        1028,
        1029)
        AND LENGTH(c.src) > 3
        AND c.billsec >= '".$seg."'
        AND c.disposition = 'ANSWERED'
GROUP BY c.dst
ORDER BY AVG(c.billsec) ASC , SUM(c.billsec) ASC , count(*) ASC
LIMIT 0 , 10";



	$res = mysqli_query($link, $sql3);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data["TOP"][] = $row;
	}
	mysqli_free_result($res);
	mysqli_close($link);

	return $data;

}

function issuesSac($opcionMesa){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

        if($opcionMesa == 'edu')
        {
                $whereMesa = "AND (`prj`.`prj_id` NOT IN (3,4,5,6,14,15))";
        }
        else
        {
                if($opcionMesa == 'salud')
                {
                        $whereMesa = "AND (`prj`.`prj_id`  = 15)";
                }
        }

	$data = array();

	$sql1 = 'SELECT count(iss.iss_id) as cantidadSAC
			 FROM
			 ((((((((((`eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			 LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			 LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			 LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			 LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			 LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`seccion`.`icf_fld_id` = 4))))
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`rbd`.`icf_fld_id` = 1))))
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			 AND (`seccion_option`.`cfo_fld_id` = 4))))
			 LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			 WHERE
			 `iss`.`iss_private` = 0
			  '.$whereMesa.'
			 AND  `st`.`sta_id` = 4;';
	$res = mysqli_query($link, $sql1);
	//print_r($sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['totalSAC'] = $row["cantidadSAC"] ;
	}
	mysqli_free_result($res);
	mysqli_close($link);
//    echo "<pre>";
//    print_r($data);
//    echo "</pre>";
	return $data;

}

function issuesSacPorAgente($opcionMesa){
	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		//$whereMesa = "AND u.usr_id not in (120,125,117,121)";
		$whereMesa =  "AND (`prj`.`prj_id` NOT IN (3,4,6,15))";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			//$whereMesa = "AND u.usr_id in (120,125,117,121)";
			$whereMesa = "AND (`prj`.`prj_id` = 15)";
		}
	}

	$data = array();

	$sql1 = 'SELECT `u`.`usr_full_name` AS `CREADO_POR`,count(iss.iss_id) as cantidad,group_concat(concat(iss.iss_id,"-",`st`.`sta_id`)) as `nro_issues`
			 FROM
			 ((((((((((`eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			 LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			 LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			 LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			 LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id`)))
			 LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))))))
			 WHERE
			 `iss`.`iss_private` = 0
			 AND (`prj`.`prj_id` NOT IN (3 , 4, 6))
			 AND `st`.`sta_id` in (4,2)
             '.$whereMesa.'
             group by `u`.`usr_full_name`
 			 order by count(iss.iss_id) desc;';
	$res = mysqli_query($link,$sql1);
	//print_r($sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data[] = $row ;
	}
	mysqli_free_result($res);
	mysqli_close($link);
 //   echo "<pre>";
 //   print_r($data);
 //   echo "</pre>";
	return $data;

}

function issuesHaciendo($opcionMesa){

	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();
	if($opcionMesa == 'edu')
	{
		$whereMesa = 'AND ud.usr_id not in(117,131,81)';
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = 'AND ud.usr_id in(117)';
		}
	}
	$sql1 = 'SELECT  `prj`.`prj_title` AS `Nombre_Proyecto`,`ud`.`usr_full_name` AS `ASIGNADO_A`,group_concat(iss.iss_id) as issues
			 FROM
			 ((((((((((`eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON ((`iss`.`iss_sta_id` = `st`.`sta_id`)))
			 LEFT JOIN `eventum`.`project_category` `pc` ON ((`iss`.`iss_prc_id` = `pc`.`prc_id`)))
			 LEFT JOIN `eventum`.`project_priority` `pp` ON ((`iss`.`iss_pri_id` = `pp`.`pri_id`)))
			 LEFT JOIN `eventum`.`user` `u` ON ((`u`.`usr_id` = `iss`.`iss_usr_id`)))
			 LEFT JOIN `eventum`.`issue_user` `iu` ON ((`iu`.`isu_iss_id` = `iss`.`iss_id`)))
			 LEFT JOIN `eventum`.`user` `ud` ON ((`ud`.`usr_id` = `iu`.`isu_usr_id` '.$whereMesa.')))
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON (((`seccion`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`seccion`.`icf_fld_id` = 4))))
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON (((`rbd`.`icf_iss_id` = `iss`.`iss_id`)
			 AND (`rbd`.`icf_fld_id` = 1))))
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON (((`seccion_option`.`cfo_id` = `seccion`.`icf_value`)
			 AND (`seccion_option`.`cfo_fld_id` = 4))))
			 LEFT JOIN `eventum`.`project` `prj` ON ((`prj`.`prj_id` = `iss`.`iss_prj_id`)))
			 WHERE
			 `iss`.`iss_private` = 0
			  AND (`prj`.`prj_id` NOT IN (3 , 4))
			 AND  `st`.`sta_id` = 3
			 '.$whereMesa.'

			 group by `ud`.`usr_full_name`
			 order by `prj`.`prj_id`,`ud`.`usr_full_name`  ASC;';
	$res = mysqli_query($link,$sql1);

	//print_r($sql1);exit;
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['Haciendo'][] = $row ;
	}
	mysqli_free_result($res);
	mysqli_close($link);
    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;

}


function getDatosAgente($opcionMesa){

	$link =  mysqli_connect('192.168.7.31', 'inexoos', 'inx@2016');
	mysqli_select_db($link,'eventum');

	$data = array();
	$whereMesa = '';
	if($opcionMesa == 'edu')
	{
		$whereMesa = "where llamada.device in ('Agent/1022','Agent/1021','Agent/1024','Agent/1025','Agent/1020','Agent/1023')";
	}
	else
	{
		if($opcionMesa == 'salud')
		{
			$whereMesa = "where llamada.device in ('Agent/1030','Agent/1031','Agent/1032')";
		}
	}

	$sql1 = "select llamada.Nombre,llamada.llamadas,tmo.TMO,llamada.tiempo_hablado,
			if(encuesta.encuestas is null,'0',encuesta.encuestas) encuestas,
			if(respondidas.respondidas is null,'0',respondidas.respondidas) as respondidas,
			if(r1 is null,'0',r1) as r1,
			if(r2 is null,'0',r2) as r2,
			if(round((r1+r2)/2,1) is null,'0',round((r1+r2)/2,1)) as Prom_Agente,
			if(if(encuesta.encuestas is not null,concat(round(respondidas.respondidas/encuesta.encuestas*100,2),'%'),'0') is null,'0',if(encuesta.encuestas is not null,concat(round(respondidas.respondidas/encuesta.encuestas*100,2),'%'),'0')) as Porcentaje_respondidas,
			if(round(encuesta.encuestas/llamada.llamadas*100,2) is null,'0%',concat(round(encuesta.encuestas/llamada.llamadas*100,2),'%')) as Porcentaje_derivadas

			from

			(SELECT an.device,an.agent as Nombre,count(aa.id) as llamadas,SEC_TO_TIME( SUM( TIME_TO_SEC(timediff(datetimeend,datetimeconnect)))) as tiempo_hablado FROM qstats.queue_stats_mv aa
			inner join qstats.agentnames an on an.device = aa.agent
			where date(datetime) = date(now())
			AND event in ('COMPLETEAGENT','COMPLETECALLER')
			GROUP BY aa.agent order by llamadas desc) as llamada

			left join (SELECT agent,substring(SEC_TO_TIME(AVG(TIME_TO_SEC(timediff(datetimeend,datetimeconnect)))),1,8) as TMO
			FROM qstats.queue_stats_mv
			where date(datetime) = date(now()) AND event in ('COMPLETEAGENT','COMPLETECALLER')
			group by agent) tmo on tmo.agent = llamada.device

			 left join (SELECT an.device,an.agent as Nombre,count(e.id) as encuestas FROM asteriskcdrdb.encuestas e
			inner join qstats.agentnames an on an.device = concat('Agent/',e.agente)
			where date(e.calldate) = date(now()) and e.agente != ''
			group by e.agente order by encuestas desc) as encuesta on encuesta.Nombre = llamada.Nombre

			left join (SELECT an.device,an.agent as Nombre,count(e.id) as respondidas,round(avg(e.r1),1) as r1,round(avg(e.r2),1) as r2 FROM asteriskcdrdb.encuestas e
			inner join qstats.agentnames an on an.device = concat('Agent/',e.agente)
			where date(e.calldate) = date(now()) and e.agente != ''
			and (r1 != '' OR r2 != '')
			group by e.agente order by respondidas desc) as respondidas on respondidas.device = llamada.device
			 ".$whereMesa."
			order by round(encuesta.encuestas/llamada.llamadas,2) desc;";
	$res = mysqli_query($link,$sql1);

	//print_r($sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data[] = $row ;
	}
	mysqli_free_result($res);
	mysqli_close($link);
 //   echo "<pre>";
 //   print_r($data);
 //   echo "</pre>";
	return $data;

}


function IssuesTopSeccionesBD()
{
	$link =  mysqli_connect('192.168.1.151', 'root', 'tec.wor_08');
	mysqli_select_db($link,'eventum');

	$data = array();

		$sql1 = 'SELECT  SUBSTRING_INDEX(`seccion_option`.`cfo_value`,"-",-1) as `Modulo`,count(iss.iss_id) as Cantidad,SUBSTR(group_concat(distinct(iss.iss_id)),1,17) as Issues
			 FROM
			 `eventum`.`issue` `iss`
			 LEFT JOIN `eventum`.`status` `st` ON `iss`.`iss_sta_id` = `st`.`sta_id`
			 LEFT JOIN `eventum`.`project_category` `pc` ON `iss`.`iss_prc_id` = `pc`.`prc_id`
			 LEFT JOIN `eventum`.`project_priority` `pp` ON `iss`.`iss_pri_id` = `pp`.`pri_id`
			 LEFT JOIN `eventum`.`user` `u` ON `u`.`usr_id` = `iss`.`iss_usr_id`
			 LEFT JOIN `eventum`.`issue_user` `iu` ON `iu`.`isu_iss_id` = `iss`.`iss_id`
			 LEFT JOIN `eventum`.`user` `ud` ON `ud`.`usr_id` = `iu`.`isu_usr_id`
			 LEFT JOIN `eventum`.`issue_custom_field` `seccion` ON `seccion`.`icf_iss_id` = `iss`.`iss_id`
			 AND `seccion`.`icf_fld_id` = 4
			 LEFT JOIN `eventum`.`issue_custom_field` `rbd` ON `rbd`.`icf_iss_id` = `iss`.`iss_id`
			 AND `rbd`.`icf_fld_id` = 1
			 LEFT JOIN `eventum`.`custom_field_option` `seccion_option` ON `seccion_option`.`cfo_id` = `seccion`.`icf_value`
			 AND `seccion_option`.`cfo_fld_id` = 4
			 LEFT JOIN `eventum`.`project` `prj` ON `prj`.`prj_id` = `iss`.`iss_prj_id`
			 WHERE
			 `iss`.`iss_private` = 0
			  AND `prj`.`prj_id` = 1
			 AND  `st`.`sta_id` not in (5,6)
			  AND pc.prc_title = "Error (Bug)"
			 group by prj.prj_id,SUBSTRING_INDEX(`seccion_option`.`cfo_value`,"-",-1)
             having cantidad >= 5 or (Modulo like "%Libro%" OR Modulo like "%Recaudaci%" OR Modulo like "%Informe%")
			 order by count(iss.iss_id) desc, Modulo asc;';

	$res = mysqli_query($link,$sql1);

	//print_r($sql1);
	while ( $row = mysqli_fetch_array ( $res) ) {

		$data['IssuesTopSecciones'][] = $row ;
	}
	mysqli_free_result($res);
	mysqli_close($link);
    //echo "<pre>";
    //print_r($data);
    //echo "</pre>";
	return $data;
}
