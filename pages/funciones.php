<?php
function UsuariosConectados(){

	$params = array();

	require_once 'conexiones.php';
	$dia=date("Y-m-d");
	$arrData = select_usuarios_conectados($dia);

	$arrValues1 = implode(",", $arrData["RES1"]);
	$arrValues2 = implode(",", $arrData["RES2"]);
	$params["ARRVALUES1"] = $arrValues1;
	$params["ARRVALUES2"] = $arrValues2;

	$arrData = $arrData["RES1"];

	$maxsnd = 0;
	$horasnd = '';
	$maxmateo= 0;
	$horamateo = '';
	$snd = 0;
	$mateo = 0;
	$apo = 0;
	$maxapo= 0;
	$horaapo = '';

	$maxarr = (count($arrData)) - 1 ;

	unset($arrData[$maxarr]);
	foreach ($arrData as $key => $data)
	{
		$arr=explode(",",preg_replace('/\[|]|\'/', '', $data));
//echo "<pre>";
//		 		print_r($arr);
//echo "</pre>";

		$sumasnd = $arr[1];
		if($sumasnd >= $maxsnd)
		{
			$maxsnd = $sumasnd;
			$horasnd = $arr[0];
			$snd = $arr[1];
		}

		$sumamateo = $arr[2];
		if($sumamateo >= $maxmateo)
		{
			$maxmateo = $sumamateo;
			$horamateo = $arr[0];
			$snd = $arr[2];
		}

		$sumaapo = $arr[3];
		if($sumaapo >= $maxapo)
		{
			$maxapo = $sumaapo;
			$horaapo = $arr[0];
			$snd = $arr[3];
		}

	}

	if(empty($arrValues)){
		$errMsj= "<strong>No se reciben Datos</strong>";
		$err=true;
	}

	//$arrUltimo=explode(",",preg_replace('/\[|]|\'/', '', $arrData[count($arrData)-1]));
	$arrUltimo=explode(",",preg_replace('/\[|]|\'/', '', $arrData[count($arrData)-2]));

	$params["LAST_SND"] = $arrUltimo[1];
	$params["MAX_SND"] = $maxsnd;
	$params["TIME_MAX_SND"] = $horasnd;

	$params["LAST_MATEO"] = $arrUltimo[2];
	$params["MAX_MATEO"] = $maxmateo;
	$params["TIME_MAX_MATEO"] = $horamateo;

// 	echo "<pre>";
// 	print_r($params);
// 	echo "</pre>";


	return $params;
}

function getLlamadas(){
	require_once 'conexiones.php';
	$seg = 30;
	$params = array();
	$dia=date("Y-m-d");
	$params = asterisk($dia,$seg);
	return $params;
}

function getIssuesSAC($opcionMesa){
	require_once 'conexiones.php';
	$datosIssuesSAC = issuesSac($opcionMesa);
	return $datosIssuesSAC;
}

function getIssuesSacPorAgente($opcionMesa){
	require_once 'conexiones.php';
	$datosIssuesSacPorAgente = issuesSacPorAgente($opcionMesa);
	//print_r($datosIssuesSacPorAgente);
	return $datosIssuesSacPorAgente;
}

function getIssuesHaciendo($opcionMesa){
	require_once 'conexiones.php';
	$issuesHaciendo = issuesHaciendo($opcionMesa);
	return $issuesHaciendo;
}

function getDatosAgenteMesa($opcionMesa){
	require_once 'conexiones.php';
	$datosAgente = getDatosAgente($opcionMesa);

	if(is_array($datosAgente))
	{
		$metricaNota = 0;
		foreach ($datosAgente as $key => $agente) {
			if($agente['encuestas'])
			{
				if($agente['respondidas'])
				{
					$metricaNota = (100/7)*$agente['r1'];
				}
				else
				{
					$metricaNota = 100;
				}
			}
			$metricaAgente[$key]['Datos'] = $agente;
			$metricaNotaAgente = round($metricaNota,2)*0.1;
			$metricaAgente[$key]['Nota'] = $metricaNotaAgente;

			if($agente['TMO']<= '00:04:05' && $agente['TMO'] >='00:03:00' )
				$metricaTMOagente = 100*0.5;
			if($agente['TMO']< '00:03:00' && $agente['TMO'] >= '00:02:30' )
				$metricaTMOagente = 95*0.5;
			if($agente['TMO']< '00:02:30' && $agente['TMO'] >= '00:02:00' )
				$metricaTMOagente = 90*0.5;
			if($agente['TMO']< '00:02:00' && $agente['TMO'] >= '00:01:00' )
				$metricaTMOagente = 60*0.5;
			if($agente['TMO']< '00:01:00' && $agente['TMO'] >= '00:00:00' )
				$metricaTMOagente = 10*0.5;

			if($agente['TMO']<= '00:04:30' && $agente['TMO'] > '00:04:05' )
				$metricaTMOagente = 85*0.5;

			if($agente['TMO']< '00:06:00' && $agente['TMO'] > '00:04:30' )
			{
				$time    = explode(':', $agente['TMO']);
				$minutes = ($time[0] * 60.0 + $time[1] * 1.0 + $time[2]/60);
				//$minutes = floor($agente['TMO'] / 60 % 60);
				//print_r("<br>".$agente['TMO'].'->'.$minutes.'<-');
				$metricaTMOagente = ((-75*($minutes-4.5)/1.5)+85)*0.5;
				//print_r($metricaNotaAgente.'+'.$metricaTMOagente)
				//print_r("<br>".$metricaTMOagente);
			}
			if($agente['TMO'] >= '00:06:00')
				$metricaTMOagente = 10*0.5;

			$metricaAgente[$key]['TMO'] = $metricaTMOagente;

			$metricaDerivacionEncuesta = $agente['Porcentaje_derivadas']*0.4;

			$metricaAgente[$key]['DerivacionEncuesta'] = $metricaDerivacionEncuesta;
			$metricaAgente[$key]['Ranking'] = $metricaNotaAgente + $metricaTMOagente + $metricaDerivacionEncuesta;
		}
	}

	$arrOrdenadoMetricaAgente = orderMultiDimensionalArray($metricaAgente,'Ranking',true);
	return $arrOrdenadoMetricaAgente;
}

function orderMultiDimensionalArray ($toOrderArray, $field, $inverse = false) {
    $position = array();
    $newRow = array();
    foreach ($toOrderArray as $key => $row) {
            $position[$key]  = $row[$field];
            $newRow[$key] = $row;
    }
    if ($inverse) {
        arsort($position);
    }
    else {
        asort($position);
    }
    $returnArray = array();
    foreach ($position as $key => $pos) {
        $returnArray[] = $newRow[$key];
    }
    return $returnArray;
}

function convertTime($dec)
{
    // start by converting to seconds
    $seconds = ($dec * 3600);
    // we're given hours, so let's get those the easy way
    $hours = floor($dec);
    // since we've "calculated" hours, let's remove them from the seconds variable
    $seconds -= $hours * 3600;
    // calculate minutes left
    $minutes = floor($seconds / 60);
    // remove those from seconds as well
    $seconds -= $minutes * 60;
    $seconds = round($seconds);
    // return the time formatted HH:MM:SS
    return lz($hours).":".lz($minutes).":".lz($seconds);
}

// lz = leading zero
function lz($num)
{
    return (strlen($num) < 2) ? "0{$num}" : $num;
}

function getIssuesTopSecciones(){
	require_once 'conexiones.php';
	$plazoIssues = IssuesTopSeccionesBD();
	return ($plazoIssues);
}
