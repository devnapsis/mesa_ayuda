<?php 
// echo "<pre>";
// print_r($_SERVER);
// echo "</pre>";
// date_default_timezone_set('America/Santiago');
setlocale(LC_ALL,"es_ES");

error_reporting(E_ERROR);

include_once 'funciones.php';

$usuariosConectados = UsuariosConectados();

$seg = 30;//tiempo desde
$llamadas = getLlamadas();


// echo "<pre>";
// print_r($llamadas);
// echo "</pre>";

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<META HTTP-EQUIV="REFRESH" CONTENT="90" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Napsis - Mesa Ayuda</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper" style="margin: 10px 0px;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mesa Ayuda - Hoy <?php echo utf8_encode(strftime("%A, %d de %B de %Y")) ?> <span id="liveclock" style="position:absolute;left:0;top:0;"></span>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ($llamadas["COUNT_LLAMADAS"]["ANSWERED"] > 0)?$llamadas["COUNT_LLAMADAS"]["ANSWERED"]:0?></div>
                                    <div>Llamadas Contestadas</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ($llamadas["COUNT_LLAMADAS"]["NO ANSWER"] > 0)?$llamadas["COUNT_LLAMADAS"]["NO ANSWER"]:0?></div>
                                    <div>Llamadas Perdidas</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="../logo-napsis-heade.png"  height="65px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ucwords($usuariosConectados["LAST_SND"])?></div>
                                    <div>Usuarios Actuales (SND)</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><bold>Max. usuarios <?php echo $usuariosConectados["MAX_SND"]." (".$usuariosConectados["TIME_MAX_SND"].")";?></bold></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="../logomnet.png"  height="65px" />
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ucwords($usuariosConectados["LAST_MATEO"])?></div>
                                    <div>Usuarios Actuales (MNET)</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><bold>Max. usuarios <?php echo $usuariosConectados["MAX_MATEO"]." (".$usuariosConectados["TIME_MAX_MATEO"].")";?></bold></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-7">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Tipos de llamadas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nro</th>
                                                    <th>Tipo</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <?php 
                                            $x = 1;
                                            foreach ($llamadas["COUNT_LLAMADAS"] as $tipo => $cant){
                                            ?>	
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $x;?></td>
                                                    <td><?php echo $tipo; ?></td>
                                                    <td><?php echo $cant; ?></td>
                                                </tr>
                                            </tbody>	
                                            	
                                            <?php $x++; } ?>
                                            
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Usuarios Conectados
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart-usuarios"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <!-- /.panel -->
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> TOP llamadas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                            
                            <?php 
                            	$suma = 0;
                            	$color =array("1"=>"green", "2"=>"orange");
                            	$x = 1;
                            	foreach ($llamadas["TOP"] as $data){ 
									
								if (isset($color[$x])) {
									$colr = "color:".$color[$x];
								}else{
									$colr = "";
								}
								$x++;
								$suma += $data["Total"];
							?>
                            	
                            	
                            	
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> <?php echo html_entity_decode("<b style='font-size: large;".$colr."'>".$data["Nombres"]. "</b> <b>Total Min.". round($data["Minutos"])." Total Llamadas ".round($data["Total"]." </b> "))?>
                                    <span class="pull-right text-muted small"><em>Promedio por minuto <?php echo $data["Promedio"]?> min.</em>
                                    </span>
                                </a>
                            	
                           <?php  }
                           
                           	$ans = ($llamadas["COUNT_LLAMADAS"]["ANSWERED"] > 0)?$llamadas["COUNT_LLAMADAS"]["ANSWERED"]:0;
                           	$menos_seg = $ans - $suma;
                           	?>
                           		<a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Llamadas de menos de <?php echo $seg." Seg. : Total "; echo $menos_seg?> 
                                    <span class="pull-right text-muted small"><em>Promedio por minuto <?php echo $data["Promedio"]?> min.</em>
                                    </span>
                                </a>
                               
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Usuarios
                        </div>
                        <div class="panel-body">
                            <div id="morris-donut-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script type="text/javascript">
    $(function() {
    	Morris.Line({
            element: 'morris-area-chart-usuarios',
            data: [<?php echo $usuariosConectados["ARRVALUES2"]?>],
            xkey: 'hora',
            ykeys: ['napsis', 'mateonet', 'apoderados'],
            labels: ['Napsis', 'MatenoNet', 'Apoderados'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });

    	Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
                label: "SND",
                value: <?php echo $usuariosConectados["LAST_SND"]; ?>
            }, {
                label: "MateoNet",
                value: <?php echo $usuariosConectados["LAST_MATEO"]; ?>
            }],
            colors: ['#f0ad4e','#337ab7'],
            resize: true
        });


    	Morris.Bar({
            element: 'morris-bar-chart',
            data: [<?php echo $llamadas["ALL_CALL"]?>],
            xkey: 'y',
            ykeys: ['a','b','c'],
            labels: ['Todos','No Contestadas', 'Contestadas'],
            hideHover: 'auto',
            resize: true
        });

    	
    });


    </script>
    <script language="JavaScript" type="text/javascript">
    function show5(){
        if (!document.layers&&!document.all&&!document.getElementById)
        return

         var Digital=new Date()
         var hours=Digital.getHours()
         var minutes=Digital.getMinutes()
         var seconds=Digital.getSeconds()

        var dn="PM"
        if (hours<12)
        dn="AM"
        if (hours>12)
        hours=hours-12
        if (hours==0)
        hours=12

         if (minutes<=9)
         minutes="0"+minutes
         if (seconds<=9)
         seconds="0"+seconds
        //change font size here to your desire
        myclock="<font size='5' face='Arial' >"+hours+":"+minutes+":"
         +seconds+" "+dn+"</font>"
        if (document.layers){
        document.layers.liveclock.document.write(myclock)
        document.layers.liveclock.document.close()
        }
        else if (document.all)
        liveclock.innerHTML=myclock
        else if (document.getElementById)
        document.getElementById("liveclock").innerHTML=myclock
        setTimeout("show5()",1000)
         }


        window.onload=show5
         //-->
     </script>
<script src="../js/morris-data.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
