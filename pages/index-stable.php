<?php 

// echo "<pre>";
// print_r($_SERVER);
// echo "</pre>";
// date_default_timezone_set('America/Santiago');
setlocale(LC_ALL,"es_ES");

error_reporting(E_ERROR);

include_once 'funciones.php';

$usuariosConectados = UsuariosConectados();
//print_r($_REQUEST);
if ($_REQUEST["mesa"])
{
    $opcionMesa = $_REQUEST["mesa"];
}

$seg = 30;//tiempo desde
$llamadas = getLlamadas();
$issuesSAC = getIssuesSAC();
$issuesSacPorAgente = getIssuesSacPorAgente($opcionMesa);
$issuesHaciendo = getIssuesHaciendo($opcionMesa);
$datosAgentes = getDatosAgenteMesa($opcionMesa);

// echo "<pre>";
// print_r($issuesHaciendo);
// echo "</pre>";
// exit;
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<META HTTP-EQUIV="REFRESH" CONTENT="90" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Napsis - Mesa Ayuda</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper" style="margin: 10px 0px;">
            <div class="row" style="margin-top: -20px;">
                <div class="col-lg-14">
                    <h1 class="page-header">Mesa Ayuda - Hoy <?php echo utf8_encode(strftime("%A, %d de %B de %Y")) ?> 
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card card-inverse card-success">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3 text-right">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ($llamadas["COUNT_LLAMADAS"]["ANSWERED"] > 0)?$llamadas["COUNT_LLAMADAS"]["ANSWERED"]:0?></div>
                                    <div>Llamadas Contestadas</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-10">
                    <div class="card card-inverse card-danger">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3 text-right" >
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo ($llamadas["COUNT_LLAMADAS"]["NO ANSWER"] > 0)?$llamadas["COUNT_LLAMADAS"]["NO ANSWER"]:0?></div>
                                    <div>Llamadas Perdidas</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-10">
                    <div class="card card-inverse card-primary">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">
                                  <!--  <img src="../img/mesa2.png"  height="65px" /> -->
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><span class="badge badge-pill badge-warning" style='font-size: 40px;'><?php echo ucwords($usuariosConectados["LAST_SND"])?></span>&nbsp;<span class="badge badge-pill badge-info" style='font-size: 40px;'><?php echo ucwords($usuariosConectados["LAST_MATEO"])?></span></div>
                                    <bold>SND <?php echo $usuariosConectados["MAX_SND"]." (".$usuariosConectados["TIME_MAX_SND"].")";?></bold>
                                    <bold>Mnet<?php echo $usuariosConectados["MAX_MATEO"]." (".$usuariosConectados["TIME_MAX_MATEO"].")";?></bold>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="card card-inverse card-warning">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-xs-3">
                                   <img src="../img/mesa2.png"  height="65px" /> 
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $issuesSAC['totalSAC']; ?></div>
                                    <div style="font-size:20px;"><bold>Issues Pendientes SAC</bold></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-5">
                    
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Pendientes de Requerimiento o SAC </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php 
                                            $x = 1;
                                            $cantSac = count($issuesSacPorAgente);
                                            if($cantSac)
                                            {
                                            if(is_array($issuesSacPorAgente))
                                            {
                                            foreach ($issuesSacPorAgente as $index => $datos){
                                            if($datos['cantidad'] >=0 && $datos['cantidad'] <= 2)
                                                $estadoColOps = 'success';
                                            if($datos['cantidad'] >=3 && $datos['cantidad'] <= 4)
                                                $estadoColOps = 'warning';
                                            if($datos['cantidad'] >=5)
                                                $estadoColOps = 'danger';
                                            $nombre = explode(' ',$datos['CREADO_POR']);
                                            ?>  
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td style="font-size:30px;"><?php echo utf8_encode($nombre['0']); ?></td>
                                                    <td style="font-size:30px;"><?php echo $datos['cantidad']; ?></td>
                                                    <td style="font-size:25px;">
                                                        <?php 
                                                        $arrIssues = explode(',',$datos['nro_issues']);
                                                        foreach ($arrIssues as $key => $nro_issueConEstado) {
                                                            $arrIssueState = explode('-', $nro_issueConEstado);
                                                            $nro_issue = $arrIssueState[0];
                                                            $estadoIssue = $arrIssueState[1]; 
                                                            if($estadoIssue == 2)
                                                            {
                                                                $colorEstadoIssue = 'success';
                                                            }
                                                            else
                                                            {
                                                                $colorEstadoIssue = 'warning';
                                                            }
                                                        ?>
                                                        <span class="badge badge-pill badge-<?php echo $colorEstadoIssue?>" style='font-size: 40px;' ><?php echo $nro_issue; ?></span>
                                                        <?php 
                                                            }
                                                        ?> 
                                                        </td>
                                                </tr>
                                            </tbody>    
                                                
                                            <?php } }
                                        }else
                                        {
                                            echo "<tbody><tr><td>";
                                            echo "<img src='../img/felicitaciones.jpg'>";
                                            echo "</td></tr></tbody>";
                                        }
                                        ?>
                                            
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>

<div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title"><i class="fa fa-bar-chart-o fa-fw"></i> Issues en Implementaci&oacute;n Operaciones</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <?php 
                                            $x = 1;
                                            $cantImpOps = count($issuesHaciendo);
                                            //$cantImpOps=0;
                                            if($cantImpOps)
                                            {
                                            if(is_array($issuesHaciendo))
                                            {
                                            foreach ($issuesHaciendo['Haciendo'] as $index => $datos){
                                                $arrIssues = explode(',', $datos['issues']);
                                                $cantidad = count($arrIssues);
                                            if($cantidad >=0 && $cantidad <= 2)
                                                $estadoColOps = 'success';
                                            if($cantidad >=3 && $cantidad <= 4)
                                                $estadoColOps = 'warning';
                                            if($cantidad >=5)
                                                $estadoColOps = 'danger';
                                            $nombre = explode(' ',$datos['ASIGNADO_A']);
                                            ?>  
                                            <tbody>
                                                <tr class="table-<?php echo $estadoColOps?>">
                                                    <td style="font-size:30px;"><?php echo utf8_encode($nombre['0']); ?></td>
                                                    <td style="font-size:30px;"><?php echo $cantidad; ?></td>
                                                    <td style="font-size:25px;">
                                                        <?php if(is_array($arrIssues))
                                                        foreach ($arrIssues as $key => $nro_issue) { ?>
                                                        <span class="badge badge-pill badge-info" style='font-size: 40px;' ><?php echo $nro_issue; ?></span>
                                                        <?php }?>
                                                        </td>
                                                </tr>
                                            </tbody>    
                                                
                                            <?php } }
                                        }else
                                        {
                                            echo "<tbody><tr><td align='center'>";
                                            echo "<img src='../img/descansando.jpg' width='500px' > ";
                                            echo "</td></tr></tbody>";
                                        }
                                        ?>
                                            
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->

                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                    </div>


                    <div class="card card-default">
                        <div class="card-header">
                           <h3 class="card-title"> <i class="fa fa-bar-chart-o fa-fw"></i> Usuarios Conectados</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="morris-area-chart-usuarios" style="height: 300px"></div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- /.card -->
                    <!-- /.card -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-7">
                    <div class="card card-default">
                        <div class="card-header">
                             <h3 class="card-title"><i class="fa fa-bell fa-fw"></i>Ranking llamadas</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="list-group">
                            
                            <?php 
                            	$suma = 0;
                            	//$color =array("1"=>"green", "2"=>"orange");
                            	$x = 1;
                                $total = count($datosAgentes);
                                $menorTreintaSegundos = 0;
                                $cantConsiderada=0;
                            	foreach ($datosAgentes as $key => $data){ 
                                    $arrTiempoHabladoMesa = explode(':',$data['Datos']["tiempo_hablado"] );
								$sumaHoras += $arrTiempoHabladoMesa[0];
                                $sumaMinutos += $arrTiempoHabladoMesa[1];
                                $sumaSegundos += $arrTiempoHabladoMesa[2]; 
                                $sumaLlamadasMesa += $data['Datos']["llamadas"];
                                $sumaEncuestasMesa += $data["Datos"]["encuestas"]; 
                                if($data['Datos']['r1'])
                                {
                                    $sumaNotasAgenteMesa += $data['Datos']['r1'];
                                    $cantConsiderada++;
                                }
                                
							
								$hora = date("H:i");
								if($hora >= "18:00"){
									if($x == 1){
										$icon = '<img alt="" src="Medalla_'.$x.'.png" width="50px">';
									}elseif ($x == 2){
										$icon = '<img alt="" src="Medalla_'.$x.'.png" width="50px">';
									}elseif ($x == 3){
										$icon = '<img alt="" src="Medalla_'.$x.'.png" width="50px">';
									}else{
										$icon = '<img alt="" src="jejeje.jpg" width="50px">';
									}
									
								}else{
									$icon = '<i class="fa fa-comment fa-fw"></i>';
								}
								
								if (isset($color[$x])) {
									$colr = "color:".$color[$x];
								}else{
									$colr = "";
								}
								
								$x++;
								$suma += $data["Total"];
                            if($data["Ranking"] >= 85)
                                $colorAlertaAgente='success';
                            if($data["Ranking"] >= 70 && $data["Ranking"] < 85)
                                $colorAlertaAgente='warning';
                            if($data["Ranking"] < 70)
                                $colorAlertaAgente='danger';

                            $nombre = explode(' ',$data['Datos']["Nombre"]);
                            $tmoAgente = explode(':', $data['Datos']['TMO']);

                            if($data['Datos']['TMO'] >= '00:02:00' && $data['Datos']['TMO'] <= '00:04:05')
                            {
                                $colorTMO = "success";
                            }
                            
                            if(($data['Datos']['TMO'] > '00:04:05' && $data['Datos']['TMO'] <= '00:04:30') || ($data['Datos']['TMO'] >= '00:00:00' && $data['Datos']['TMO'] < '00:02:00'))
                            {
                                 $colorTMO = 'warning';
                            }
                            
                            if($data['Datos']['TMO'] > '00:04:30')
                            {
                               $colorTMO = 'danger';
                            }
                            if ($data['Datos']['r1']>='1' && $data['Datos']['r1'] < '4')
                            {
                                $colorNota = 'danger';
                            }
                            if ($data['Datos']['r1']>='4' && $data['Datos']['r1'] < '6')
                            {
                                $colorNota = 'warning';
                            }
                            if (($data['Datos']['r1']>='6' && $data['Datos']['r1'] <= '7') || $data['Datos']['r1'] == '0')
                            {
                                $colorNota = 'success';
                            }

                            if($data['Datos']['Porcentaje_derivadas'] >= 0 && $data['Datos']['Porcentaje_derivadas'] < 50)
                            {
                                $colorEncuestas = 'danger';
                            }
                            if($data['Datos']['Porcentaje_derivadas'] >= 50 && $data['Datos']['Porcentaje_derivadas'] < 85)
                            {
                                $colorEncuestas = 'warning';
                            }
                            if($data['Datos']['Porcentaje_derivadas'] >= 85 && $data['Datos']['Porcentaje_derivadas'] <= 100)
                            {
                                $colorEncuestas = 'success';
                            }

							?>
                            	
                            	
                            	
                                 <!--<a href="#" class="list-group-item">-->
                                    <div class="alert alert-<?php echo $colorAlertaAgente?>"><?php echo $icon; ?><?php echo "<b style='font-size: 35px;".$colr."'>".utf8_encode($nombre[0]). "&nbsp;<span class='badge badge-pill badge-".$colorAlertaAgente."' style='font-size: 40px;'>".round($data["Ranking"],1)."%</span></b> <b style='font-size: 15px;".$colr."'>
                                    TT ". $data['Datos']["tiempo_hablado"]." LL: ".round($data['Datos']["llamadas"])." E:".$data["Datos"]["encuestas"]."</b>";?>
                                    <span class="badge badge-pill badge-<?php echo $colorTMO;?>" style='font-size: 40px;' ><?php echo $tmoAgente[1].':'.$tmoAgente[2]?></span>
                                    <span class="badge badge-pill badge-<?php echo $colorNota;?>" style='font-size: 40px;' ><?php echo $data['Datos']['r1']?></span>
                                    <span class="badge badge-pill badge-<?php echo $colorEncuestas;?>" style='font-size: 40px;' ><?php echo round($data['Datos']['Porcentaje_derivadas'],1)?>%</span>
                                  <!--  <span class="badge" style='font-size: 30px; ' ><?php echo round($data["Ranking"],2)?></span> -->
                                    </div>
                                <!--</a>-->
                            	
                           <?php  }
                           //print_r($sumaHoras.'-'.$sumaMinutos.'-'.$sumaSegundos.'<br>');
                           $minutosHabladosMesa = $sumaHoras*60 + $sumaMinutos + $sumaSegundos/60;
                           $promMesa = $minutosHabladosMesa / ($sumaLlamadasMesa);
                           $promNotaMesa = ($cantConsiderada)?$sumaNotasAgenteMesa/$cantConsiderada:'0';
                           $promEncuestasDerivadasMesa = ($sumaLlamadasMesa)?$sumaEncuestasMesa / $sumaLlamadasMesa:'0' ;
                           //  print_r($minutosHabladosMesa.'-'.$sumaLlamadasMesa.'-'.$promMesa.'-'.convertTime($promMesa));

                           	//$ans = ($llamadas["COUNT_LLAMADAS"]["ANSWERED"] > 0)?$llamadas["COUNT_LLAMADAS"]["ANSWERED"]:0;
                           	//$menos_seg = $menorTreintaSegundos;
                            if($promMesa < 3.5)                         {$colorAlerta='success';}
                            if($promMesa >= 3.5 && $promMesa <= 4.5)    {$colorAlerta='success';}
                            if($promMesa >= 4.5 && $promMesa <= 5.000)    {$colorAlerta='warning';}
                            if($promMesa > 5.000)                         {$colorAlerta='danger';}
                           	?>
                           		<!-- 
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Llamadas de menos de <?php echo $seg." Seg. : Total "; echo $menos_seg?> 
                                </a>
                                -->
                                    <div class="alert alert-<?php echo $colorAlerta?>" align="right">
                                    <h1>
                                        <i class="fa fa-comment fa-fw"></i>MESA <span style='font-size: 40px;' class='badge badge-<?php echo $colorAlerta; ?>'><?php echo convertTime($promMesa/60); ?></span>
                                       <span style='font-size: 40px;' class='badge badge-pill badge-info' ><?php echo number_format(round($promNotaMesa,1),1); ?></span>
                                       <span style='font-size: 40px;' class='badge badge-info' ><?php echo round($promEncuestasDerivadasMesa*100,1,3); ?>%</span>
                                        </h1>
                                        Total Llamadas Mesa: <span style='font-size: 20px;' class='badge badge-pill badge-default' ><?php echo $sumaLlamadasMesa; ?></span>
                                        Total Encuestas Mesa: <span style='font-size: 20px;' class='badge badge-pill badge-default' ><?php echo $sumaEncuestasMesa; ?></span>
                                        Tiempo Hablados Mesa: <span style='font-size: 20px;' class='badge badge-pill badge-default' ><?php echo convertTime($minutosHabladosMesa/60); ?></span>
                                   </div>
                               

                               
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script type="text/javascript">
    $(function() {
    	Morris.Line({
            element: 'morris-area-chart-usuarios',
            data: [<?php echo $usuariosConectados["ARRVALUES2"]?>],
            xkey: 'hora',
            ykeys: ['napsis', 'mateonet', 'apoderados'],
            labels: ['Napsis', 'MatenoNet', 'Apoderados'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });

    	Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
                label: "SND",
                value: <?php echo $usuariosConectados["LAST_SND"]; ?>
            }, {
                label: "MateoNet",
                value: <?php echo $usuariosConectados["LAST_MATEO"]; ?>
            }],
            colors: ['#f0ad4e','#337ab7'],
            resize: true
        });


    	Morris.Bar({
            element: 'morris-bar-chart',
            data: [<?php echo $llamadas["ALL_CALL"]?>],
            xkey: 'y',
            ykeys: ['a','b','c'],
            labels: ['Todos','No Contestadas', 'Contestadas'],
            hideHover: 'auto',
            resize: true
        });

    	
    });


    </script>
    <script language="JavaScript" type="text/javascript">
    function show5(){
        if (!document.layers&&!document.all&&!document.getElementById)
        return

         var Digital=new Date()
         var hours=Digital.getHours()
         var minutes=Digital.getMinutes()
         var seconds=Digital.getSeconds()

        var dn="PM"
        if (hours<12)
        dn="AM"
        if (hours>12)
        hours=hours-12
        if (hours==0)
        hours=12

         if (minutes<=9)
         minutes="0"+minutes
         if (seconds<=9)
         seconds="0"+seconds
        //change font size here to your desire
        myclock="<font size='5' face='Arial' >"+hours+":"+minutes+":"
         +seconds+" "+dn+"</font>"
        if (document.layers){
        document.layers.liveclock.document.write(myclock)
        document.layers.liveclock.document.close()
        }
        else if (document.all)
        liveclock.innerHTML=myclock
        else if (document.getElementById)
        document.getElementById("liveclock").innerHTML=myclock
        setTimeout("show5()",1000)
         }


        window.onload=show5
         //-->
     </script>
<script src="../js/morris-data.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>

